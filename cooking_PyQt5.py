from PyQt5.QtWidgets import (QApplication, QWidget, 
                             QGridLayout, QPushButton)

import numpy as np
import sys

import matplotlib
# for full compatibility we use the specific render
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


class PastaWithTomatoSauce(QWidget):

    def __init__(self, parent=None):
        super(PastaWithTomatoSauce, self).__init__(parent)

        # create figure, canvas and toolbar objects
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        toolbar = NavigationToolbar(self.canvas, self)

        # add button objects and 
        # connect the click signal to relevant functions
        button_pasta = QPushButton('Pasta')
        button_pasta.clicked.connect(self.add_pasta)
        button_sauce = QPushButton('Sauce')
        button_sauce.clicked.connect(self.add_sauce)
        button_cheese = QPushButton('Cheese')
        button_cheese.clicked.connect(self.add_cheese)

        # create the layout and add all widgets
        # on proper positions
        layout = QGridLayout()
        layout.addWidget(button_pasta, 2, 0)
        layout.addWidget(button_sauce, 2, 1)
        layout.addWidget(button_cheese, 2, 2)
        layout.addWidget(toolbar, 0, 0, 1, 3)
        layout.addWidget(self.canvas, 1, 0, 1, 3)

        # assign the layout to self, a QWidget
        self.setLayout(layout)

    def add_pasta(self):
        # clear the canvas for a new dish
        self.figure.clear()
        
         # generate random data
        chi = np.random.uniform(0, 10, 400)
        psi = np.random.uniform(0, 10, 400)

        # create axis with relevant lengths
        ax = self.figure.add_subplot(111)
        plt.axis((0, 10, 0, 10))

        # plot data
        # set relevant color
        plt.plot(chi, psi, color='gold')

        # update canvas with current figure
        self.canvas.draw()

    def add_sauce(self):
        # generate random data
        # tomato should be on pasta so we limit the boundaries
        chi = np.random.uniform(1, 9, 40)
        psi = np.random.uniform(1, 9, 40)

        # create axis with relevant lengths
        ax = self.figure.add_subplot(111)
        plt.axis((0, 10, 0, 10))

        # plot data
        # set relevant color, marker size, and transparency
        plt.plot(chi, psi, 'o', color='red', ms=35, alpha=0.5)

        # update canvas with current figure
        self.canvas.draw()

    def add_cheese(self):
        # generate random data
        # cheese should be on tomato so we limit the boundaries
        chi = np.random.uniform(2, 8, 40)
        psi = np.random.uniform(2, 8, 40)

        # create axis with relevant lengths
        ax = self.figure.add_subplot(111)
        plt.axis((0, 10, 0, 10))

        # plot data
        # set relevant color, marker edge color, and width
        plt.plot(chi, psi, '.', c='yellow', mec='black', mew=0.5)

        # update canvas with current figure
        self.canvas.draw()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = PastaWithTomatoSauce()
    main.show()

    sys.exit(app.exec_())
