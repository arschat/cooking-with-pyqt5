# cooking with PyQt5

This is a very simple tutorial for using the PyQt5 library in the form of an small excercise to understand some very basic principles of GUI development with this library.
Using PyQt5 and matplotlib as well as numpy and sys libraries, you can virtually cook some pasta with tomato sauce and cheese.

It was initially written as a blog post for my "Summer of HPC" 2019 participation.
https://summerofhpc.prace-ri.eu/recipe-for-a-delicious-dish/
